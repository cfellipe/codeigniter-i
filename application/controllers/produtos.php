<?php 

class Produtos extends CI_Controller {

	public function index()
	{	
		$this->load->database();//carrega o banco
		$this->load->model('produtos_model');//carrega o model
		$produtos = $this->produtos_model->buscaProdutos();//carrega a gunção dentro do model

		$dados = array("produtos" => $produtos);

		$this->load->view('produtos/index.php', $dados);
	}

}
?>